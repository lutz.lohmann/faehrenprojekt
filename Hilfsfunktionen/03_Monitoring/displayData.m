function displayData(block, ~)
%% settings
height          = 40;       % Shiplength [m] VARIABEL MACHEN
width           = 12.4;     % Shipwidth [m] VARIABEL MACHEN
topFactor       = 0.9;      % Factor for plotting the top of the Ship
DistanceFactor  = 1.8;      % Factor for distance between the plots of ships (rectangle)
maxShips        = 4;        % maximum Number of plottet ships


%% find Objects
% Input
F               = block.OutputPort(1).Data;
data            = block.OutputPort(2).Data;
tSim            = block.OutputPort(3).Data;
Soll            = block.InputPort(3).Data;

% Find handles
uGround         = findall(0,'Tag','uGround');
vGround         = findall(0,'Tag','vGround');
rGround         = findall(0,'Tag','rGround');

speedGUI        = findall(0,'Tag','GUISpeed');
forcesGUI       = findall(0,'Tag','GUIForces');

fx              = findall(0,'Tag','F_x');
fy              = findall(0,'Tag','F_y');
n               = findall(0,'Tag','N');

Heading         = findall(0,'Tag','Heading');

simTime         = findall(0,'Tag','SimTime');
calcTime        = findall(0,'Tag','calculationTime');
calcSampling    = findall(0,'Tag','calculationSamplingFactor');

Plotmap         = findall(0,'Tag','MapAxes');

ShipAct         = findall(0,'Tag','ShipAct');
ShipActRect     = findall(0,'Tag','ShipActRect');
ShipPre         = findall(0,'Tag','ShipPre');
ShipPreRect     = findall(0,'Tag','ShipPreRect');
Target          = findall(0,'Tag','Target');
TargetRect      = findall(0,'Tag','TargetRect');

%% Main Plot 
hold(Plotmap, 'on')

if isempty(ShipAct) % initial
    plot(Plotmap, data(1,1), data(1,2),'*y','Tag','ShipAct');
    rot_coordsShip      = drawShipAtAngle([data(1,1);data(1,2)], width, height, data(1,3), topFactor);
    plot(Plotmap, rot_coordsShip(1,:), rot_coordsShip(2,:),'-y', 'Tag', 'ShipActRect');
    plot(Plotmap,  data(2:end,1), data(2:end,2),'-*g', 'Tag', 'ShipPre');
    
    rot_coordTarget = drawShipAtAngle([Soll(1); Soll(2)], width, height, Soll(3), topFactor);
    plot(Plotmap, rot_coordTarget(1,:), rot_coordTarget(2,:),'-r','Tag', 'TargetRect');
    plot(Plotmap, Soll(1), Soll(2),'*r','Tag','Target');
    
    for j = 1:maxShips
        plot(Plotmap, 0, 0, '-g', 'Tag', 'ShipPreRect', 'visible',0);
    end
 
else % refresh

    ShipAct.XData       = data(1,1);
    ShipAct.YData       = data(1,2);
    
    rot_coordsShip      = drawShipAtAngle([data(1,1);data(1,2)], width, height, data(1,3), topFactor);
    ShipActRect.XData   = rot_coordsShip(1,:);
    ShipActRect.YData   = rot_coordsShip(2,:);
    
    ShipPre.XData       = data(2:end,1);
    ShipPre.YData       = data(2:end,2);
    
    %% Prediction Rectangles
    lastShip            = [data(1,1),data(1,2)];
    j                   = 1;
    for k = 2:size(data,1)
        if sqrt((data(k,2)-lastShip(2))^2+(data(k,1)-lastShip(1))^2) > DistanceFactor * max([height,width]) && j <= maxShips
            rot_coordsPre           = drawShipAtAngle([data(k,1);data(k,2)], width, height, data(k,3), topFactor); 
            ShipPreRect(j).XData    = rot_coordsPre(1,:);
            ShipPreRect(j).YData    = rot_coordsPre(2,:);
            ShipPreRect(j).Visible  = 1;
            lastShip                = [data(k,1), data(k,2)];
            j                       = j+1;
        end
    end
    % hide not needed ships
    for i = j:maxShips
        ShipPreRect(i).Visible = 0;
    end
    %% Soll trajectorie
    Target.XData        = Soll(1);
    Target.YData        = Soll(2);
    rot_coordTarget     = drawShipAtAngle([Soll(1); Soll(2)], width, height, Soll(3), topFactor);
    TargetRect.XData    = rot_coordTarget(1,:);
    TargetRect.YData    = rot_coordTarget(2,:);
end

%% Other informations/Plots
% speed GUI
LineWidthPlot   = 1.5;
tLimits         = xlim(speedGUI);
t               = linspace(tLimits(1), tLimits(2), size(data,1));
hold(speedGUI, 'off')
plot(speedGUI,t,data(:,4),'-','LineWidth',LineWidthPlot);
hold(speedGUI, 'on')
plot(speedGUI,t,data(:,5),'-','LineWidth',LineWidthPlot);
legend(speedGUI,{'u', 'v'});

% Forces GUI
hold(forcesGUI, 'off')
stairs(forcesGUI,t,data(:,7),'-','LineWidth',LineWidthPlot);
hold(forcesGUI, 'on')
stairs(forcesGUI,t,data(:,8),'-','LineWidth',LineWidthPlot);
stairs(forcesGUI,t,data(:,9),'-','LineWidth',LineWidthPlot);
legend(forcesGUI,{'F_x', 'F_y', 'N'});



uGround.Value       = data(1,4);
vGround.Value       = data(1,5);
rGround.Value       = data(1,6)*180/pi();
Heading.Value       = data(1,3)*180/pi();

simTime.Text        = num2str(tSim(1),2);
calcTime.Text       = num2str(tSim(2),3);
calcSampling.Text   = num2str(tSim(3),3);

fx.Value            = F(1)/1000;
fy.Value            = F(2)/1000;
n.Value             = F(3)/1000;

