import casadi.*


% Symbols/expressions
x = MX.sym('x');
y = MX.sym('y');

f = x^4+100*y^2;
g = x+y+1;

nlp     = struct;           % NLP declaration
nlp.x   = [x;y];            % decision vars
nlp.f   = f;                % objective
nlp.g   = g;                % constraints

H = f.hessian(x);

% Create solver instance
F = nlpsol('F','sqpmethod',nlp, struct('qpsol', 'osqp'));

% Solve the problem using a guess
Result = F('x0',[3.0 -3.0],'ubg',0,'lbg',0);
Result.x

C = CodeGenerator('Testgen.c');
C.add(F);
C.generate();