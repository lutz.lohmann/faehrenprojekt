options = Initalvalues();
import casadi.*

% x       = SX.sym('x',2);
% f       = 0.1*x(1)^3 + x(2)^2;
% H       = f.hessian(x);
% g       = f.gradient(x);
% Hfunc = Function('H',{x}, {H});
% gfunc = Function('g',{x}, {g});


x       = MX.sym('x', options.Initial.nStates);
u       = MX.sym('u', options.Initial.nInputs);


f       = Function('f',{x,u},...
                    {runVesselCas(x,u)},...
                    {'x','u'},{'xdot'});


  
C = CodeGenerator('ShipDynamics.c');
C.add(f);
C.generate();