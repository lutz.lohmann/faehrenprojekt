function vessel = initializeVessel()           
    base_path           = 'Lodi';
   % vessel.Fitted       = load('fittedFunctions.mat');
    vessel.rho_water    = 1.0; % [t/m^3]
    vessel.g            = 9.81; 
    vessel.tau3_HL      = zeros(3,1); % vector of hull forces and moments in b [X_HL,Y_HL,N_HL]^T [kN,kNm]
    vessel.tau3_CC      = zeros(3,1); % vector of Coriolis and centripedal forces and moments in b [X_HL,Y_HL,N_HL]^T [kN,kNm]
    vessel.eta3         = zeros(3,1); % vector of positions and yaw angle in NED (N,E,psi)^T [m,rad]
    vessel.nu3          = zeros(3,1); % vector of velocities and yaw rates in b (u,v,r)^T [m/s, rad/s]
    vessel.beta         = 0;  % drift angle [rad]
    vessel.nu3r         = zeros(2,1); % speed through water in b (ur,vr,0)^T [m/s]
    vessel.U            = 0; % total speed through water [m/s]
    vessel.vc           = zeros(2,1); % global current velocity in NED (uc,vc)^T [m/s]
    vessel.vw           = zeros(2,1); % global wind velocity in NED (uw,vw)^T [m/s]
    vessel.Fn           = 0; % Froude number [-]
    vessel.gamma        = 0; % yaw velocity angle [rad]
    
    vessel.xFactor      = 1/71500;
    vessel.yFactor      = 1/111300;
    
    vessel.XFN          = str2num(fileread(fullfile(base_path,'XFN.txt')));
    vessel.XBETA        = str2num(fileread(fullfile(base_path,'XBETA.txt')));
    vessel.YBETA        = str2num(fileread(fullfile(base_path,'YBETA.txt')));
    vessel.NBETA        = str2num(fileread(fullfile(base_path,'NBETA.txt')));
    vessel.XGAMMA       = str2num(fileread(fullfile(base_path,'XGAMMA.txt')));
    vessel.YGAMMA       = str2num(fileread(fullfile(base_path,'YGAMMA.txt')));
    vessel.NGAMMA       = str2num(fileread(fullfile(base_path,'NGAMMA.txt')));  
    
    Lpp                 = 81.90; % Lpp=Lwl
    c_B                 = 0.1587; % Beam mld. coef.    (B/Lpp)
    c_T                 = 0.02271; % Draught aft  coef. (Ta/Lpp
    cb                  = 0.4747; % Block coef.(/Lpp B Tm)
    c_rzz               = 0.25; % Radius of gyration (/Lpp) 
    c_ALW               = 0.7123; % Under water Lateral area coef. (/(Lpp Tm)
    c_S                 = 0.6938; % Wetted surface coef. (/(Lpp(2Tm+B))  
    vessel.c_CoG        = [0.0 0.0 -1.4893]; % Center of gravity  (/Lpp,/B,/Tm)

    vessel.B            = c_B*Lpp;
    vessel.T            = c_T*Lpp;
    CoG                 = vessel.c_CoG.*[Lpp vessel.B vessel.T];

    V                   = Lpp*vessel.B*vessel.T*cb; % displacement
    mrb                 = V*vessel.rho_water; % rigid body mass
    rzz                 = c_rzz*Lpp;
    Izz                 = mrb*rzz^2;

    Xu                  = -1.49E-04;
    Yv                  = -1.30E-03;
    Nb                  = -3.954E-05;
    MA_diag             = vessel.rho_water/2*Lpp^3*[Xu Yv Nb*Lpp^2];
    vessel.MA           = diag(MA_diag);

    vessel.ALW          = c_ALW*Lpp*vessel.T;
    vessel.S            = c_S*Lpp*(2*vessel.T+vessel.B);
    
    vessel.Lpp          = Lpp;
    vessel.CoG          = CoG(:);

    xg                  = vessel.CoG(1);
    yg                  = vessel.CoG(2);
    vessel.MRB          = [mrb 0 -mrb*yg; 0 mrb mrb*xg; -mrb*yg mrb*xg Izz];
    vessel.M            = vessel.MRB - vessel.MA;
    vessel.Minv         = inv(vessel.M);     

 %   vessel.dt           = 0.1;
end

