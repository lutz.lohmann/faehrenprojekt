function [N_Check, E_Check] = rotTransFunc(N_Schiff, E_Schiff, psi_Schiff, N_Ref, E_Ref)
N_Check = N_Schiff + N_Ref * cos(psi_Schiff) - E_Ref * sin(psi_Schiff);
E_Check = E_Schiff - N_Ref * sin(psi_Schiff) + E_Ref * cos(psi_Schiff);