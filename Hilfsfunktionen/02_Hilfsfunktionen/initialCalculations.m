function options = initialCalculations(options)
%% MPC
options.MPC.N               = floor(options.MPC.tFinal/options.MPC.Ts);             % Horizon length []
options.MPC.Nu              = floor(options.MPC.tControl/options.MPC.Ts);           % Control horizon length []
options.MPC.InitialPath     = zeros((options.MPC.N+1)*options.Initial.nStates,1);   % Inital trajectory

options.MPC.ControlVar      = zeros(options.MPC.N,1);
k = 1;
while k <= min([options.MPC.N;options.MPC.Nu])
    options.MPC.ControlVar(k)   = 1;
    k                           = k+floor(options.MPC.reduction^k);
end
options.MPC.SumControlVar = sum(options.MPC.ControlVar);

%% MPC Obstacles
% options.MPC.staticObstacles = table2struct(readtable('statischeHindernisse.csv'));
% options.MPC.numStaticObst   = length(options.MPC.staticObstacles);

%% Function fitting
fittedFunctions = load('fittedFunctions.mat');
names           = fieldnames(fittedFunctions);
for i = 1:length(names)
    options.MPC.fittedFunctions.(names{i}) = coeffvalues(fittedFunctions.(names{i}));
end

%% Path generations
if options.boolCSVPath
    dataTable = readtable('beispieltrj_ingelheim_oestrich.csv');
    T = table2dataset(dataTable);
    time_vec = [0:options.MPC.Ts:T.time_s(end)];
    POI = max((T.time_s == time_vec),[],2)';
 %   options.MPC.Trajec = table2array(dataTable(POI',:));
    options.MPC.Trajec = table2array(dataTable);
    options.Initial.p_n         = [T.E_m(1); T.N_m(1)];                    % Position NED [m]
    options.Initial.Theta_nb    = T.psi_rad(1);                        % Euler angle [rad]
    options.Initial.v_b         = [T.u_m_s(1); T.v_m_s(1)];                    % linear velocity ship [m/s]
    options.Initial.omega_b     = T.r_psi_s(1);                        % angular velocity [rad/s]
    options.Initial.x0          = [T.E_m(1); T.N_m(1); T.psi_rad(1); T.u_m_s(1); T.v_m_s(1); T.r_psi_s(1)];
end

