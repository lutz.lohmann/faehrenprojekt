function rot_coords = drawShipAtAngle(center,width, height,theta,topFactor)
% To draw a ship at a given pixel location (center) with any width and
% height rotated at an angle
% Input: 
% center        - the pixel location of the center of rectangle 2x1 vector
% width         - width of rectangle in pixels
% height        - height of rectangle in pixels
% angle         - rotation angle of rectangle in radians
% topFactor     - Factor for plotting the top of the ship
% Output: 
% rot_ccords    - rotated coordinates of the ship

coords              = [center(1)-(width/2) center(1)-(width/2) center(1) center(1)+(width/2)  center(1)+(width/2);...
               center(2)-(height/2) center(2)+topFactor*(height/2)  center(2)+(height/2) center(2)+topFactor*(height/2)  center(2)-(height/2)];
R                   = [cos(theta) sin(theta);...
                -sin(theta) cos(theta)];
rot_coords          = R*(coords-repmat(center,[1 5]))+repmat(center,[1 5]);
rot_coords(:,6)     = rot_coords(:,1);
