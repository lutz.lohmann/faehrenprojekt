function xdot = runVessel(options, x, uInput)
vessel          = options.vessel;
m               = vessel.MRB(1);
uin             = uInput/1000;
xg              = vessel.CoG(1);
yg              = vessel.CoG(2);
zg              = vessel.CoG(3);


psi             = x(3);
u               = x(4);
v               = x(5);
r               = x(6);

factorRad2Deg   = 180/pi();
actVlc          = x(4:6);


R           = [cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1];

% my motion equation 
% Mrb*nu_dot + Crb(nu)*nu + CA(nur) = tau_hull + tau_ext + tau_wind

vessel.nu3r    = actVlc-(R')*[vessel.vc;0];
vessel.U       = sqrt(abs(vessel.nu3r(1)).^2 + abs(vessel.nu3r(2)).^2);    % vessel.U= hypot(vessel.nu3r(1), vessel.nu3r(2));
vessel.Fn      = vessel.U/sqrt(vessel.g*vessel.Lpp);
vessel.beta    = atan2(v,u);
vessel.gamma   = atan2(0.5*r*vessel.Lpp,vessel.U);

dim_res         = vessel.rho_water/2*u^2*vessel.S;
dim_drift       = vessel.rho_water/2*vessel.U^2*vessel.ALW;
dim_yaw         = vessel.rho_water/2*(vessel.U^2+(r*vessel.Lpp/2)^2)*vessel.ALW;

beta_FORCE      = -vessel.beta; % defined by force: atan2(-v,u);
%% Lookup
% XHL = ...
%     interp1(vessel.XFN(:,1),vessel.XFN(:,2),vessel.Fn,'linear','extrap')*dim_res + ...
%     interp1(vessel.XBETA(:,1),vessel.XBETA(:,2),rad2deg(beta_FORCE),'linear','extrap')*dim_drift + ...
%     interp1(vessel.XGAMMA(:,1),vessel.XGAMMA(:,2),rad2deg(vessel.gamma),'linear','extrap')*dim_yaw;
% YHL = ...
%     interp1(vessel.YBETA(:,1),vessel.YBETA(:,2),rad2deg(beta_FORCE),'linear','extrap')*dim_drift + ...
%     interp1(vessel.YGAMMA(:,1),vessel.YGAMMA(:,2),rad2deg(vessel.gamma),'linear','extrap')*dim_yaw;
% NHL = ...
%     interp1(vessel.NBETA(:,1),vessel.NBETA(:,2),rad2deg(beta_FORCE),'linear','extrap')*dim_drift*vessel.Lpp + ...
%     interp1(vessel.NGAMMA(:,1),vessel.NGAMMA(:,2),rad2deg(vessel.gamma),'linear','extrap')*dim_yaw*vessel.Lpp;

%% Splines instead of Lookup
% XHL = ...
%     spline(vessel.XFN(:,1),vessel.XFN(:,2),vessel.Fn)*dim_res + ...
%     spline(vessel.XBETA(:,1),vessel.XBETA(:,2),rad2deg(beta_FORCE))*dim_drift + ...
%     spline(vessel.XGAMMA(:,1),vessel.XGAMMA(:,2),rad2deg(vessel.gamma))*dim_yaw;
% YHL = ...
%     spline(vessel.YBETA(:,1),vessel.YBETA(:,2),rad2deg(beta_FORCE))*dim_drift + ...
%     spline(vessel.YGAMMA(:,1),vessel.YGAMMA(:,2),rad2deg(vessel.gamma))*dim_yaw;
% NHL = ...
%     spline(vessel.NBETA(:,1),vessel.NBETA(:,2),rad2deg(beta_FORCE))*dim_drift*vessel.Lpp + ...
%     spline(vessel.NGAMMA(:,1),vessel.NGAMMA(:,2),rad2deg(vessel.gamma))*dim_yaw*vessel.Lpp;
% %%
% vessel.tau3_HL = [XHL; YHL; NHL];

%% Casadi Interp1 linear
XFN     = casadi.interpolant('XFN', 'linear', {vessel.XFN(:,1)}, vessel.XFN(:,2));
XBETA   = casadi.interpolant('XBETA', 'linear', {vessel.XBETA(:,1)}, vessel.XBETA(:,2));
XGAMMA  = 0;%casadi.interpolant('XGAMMA', 'linear', {vessel.XGAMMA(:,1)}, vessel.XGAMMA(:,2));     % ggf. anpassen
YBETA   = casadi.interpolant('YBETA', 'linear', {vessel.YBETA(:,1)}, vessel.YBETA(:,2));
YGAMMA  = 0; %casadi.interpolant('YGAMMA', 'linear', {vessel.YGAMMA(:,1)}, vessel.YGAMMA(:,2));    % ggf. anpassen
NBETA   = casadi.interpolant('NBETA', 'linear', {vessel.NBETA(:,1)}, vessel.NBETA(:,2));
NGAMMA  = casadi.interpolant('NGAMMA', 'linear', {vessel.NGAMMA(:,1)}, vessel.NGAMMA(:,2));

XHL = XFN(vessel.Fn) * dim_res + XBETA(factorRad2Deg*beta_FORCE) * dim_drift + XGAMMA * dim_yaw;
YHL = YBETA(factorRad2Deg * beta_FORCE) * dim_drift + YGAMMA;%YGAMMA(factorRad2Deg * vessel.gamma) * dim_yaw;
NHL = NBETA(factorRad2Deg * beta_FORCE) * dim_drift * vessel.Lpp + NGAMMA(factorRad2Deg * vessel.gamma) * dim_yaw * vessel.Lpp;

vessel.tau3_HL = [XHL; YHL; NHL];

%% Casadi Interp1 bspline
% XFN     = casadi.interpolant('XFN', 'bspline', {vessel.XFN(:,1)}, vessel.XFN(:,2));
% XBETA   = casadi.interpolant('XBETA', 'bspline', {vessel.XBETA(:,1)}, vessel.XBETA(:,2));
% XGAMMA  = 0;%casadi.interpolant('XGAMMA', 'bspline', {vessel.XGAMMA(:,1)}, vessel.XGAMMA(:,2));     % ggf. anpassen
% YBETA   = casadi.interpolant('YBETA', 'bspline', {vessel.YBETA(:,1)}, vessel.YBETA(:,2));
% YGAMMA  = 0; %casadi.interpolant('YGAMMA', 'bspline', {vessel.YGAMMA(:,1)}, vessel.YGAMMA(:,2));    % ggf. anpassen
% NBETA   = casadi.interpolant('NBETA', 'bspline', {vessel.NBETA(:,1)}, vessel.NBETA(:,2));
% NGAMMA  = casadi.interpolant('NGAMMA', 'bspline', {vessel.NGAMMA(:,1)}, vessel.NGAMMA(:,2));
% 
% XHL = XFN(vessel.Fn) * dim_res + XBETA(factorRad2Deg*beta_FORCE) * dim_drift + XGAMMA * dim_yaw;
% YHL = YBETA(factorRad2Deg * beta_FORCE) * dim_drift + YGAMMA;%YGAMMA(factorRad2Deg * vessel.gamma) * dim_yaw;
% NHL = NBETA(factorRad2Deg * beta_FORCE) * dim_drift * vessel.Lpp + NGAMMA(factorRad2Deg * vessel.gamma) * dim_yaw * vessel.Lpp;
% 
% vessel.tau3_HL = [XHL; YHL; NHL];

%% Functions
% % XFN     = casadi.interpolant('XFN', 'bspline', {vessel.XFN(:,1)}, vessel.XFN(:,2));
% XBETA   = casadi.interpolant('XBETA', 'bspline', {vessel.XBETA(:,1)}, vessel.XBETA(:,2));
%  XGAMMA  = 0;%casadi.interpolant('XGAMMA', 'bspline', {vessel.XGAMMA(:,1)}, vessel.XGAMMA(:,2));     % ggf. anpassen
% % YBETA   = casadi.interpolant('YBETA', 'bspline', {vessel.YBETA(:,1)}, vessel.YBETA(:,2));
%  YGAMMA  = 0; %casadi.interpolant('YGAMMA', 'bspline', {vessel.YGAMMA(:,1)}, vessel.YGAMMA(:,2));    % ggf. anpassen
% % NBETA   = casadi.interpolant('NBETA', 'bspline', {vessel.NBETA(:,1)}, vessel.NBETA(:,2));
% % NGAMMA  = casadi.interpolant('NGAMMA', 'bspline', {vessel.NGAMMA(:,1)}, vessel.NGAMMA(:,2));
% 
% XHL = polyval(options.MPC.fittedFunctions.XFnPos',vessel.Fn) * dim_res + XBETA(factorRad2Deg*beta_FORCE) * dim_drift + XGAMMA * dim_yaw;
% YHL = polyval(options.MPC.fittedFunctions.YBeta',factorRad2Deg * beta_FORCE) * dim_drift + YGAMMA;%YGAMMA(factorRad2Deg * vessel.gamma) * dim_yaw;
% NHL = polyval(options.MPC.fittedFunctions.NBeta', factorRad2Deg * beta_FORCE) * dim_drift * vessel.Lpp + polyval(options.MPC.fittedFunctions.NGamma', factorRad2Deg * vessel.gamma) * dim_yaw * vessel.Lpp;
% 
% vessel.tau3_HL = [XHL; YHL; NHL];

%%
% Coriolis & centripetal forces
ur  = vessel.nu3r(1);
vr  = vessel.nu3r(2);
rr  = vessel.nu3r(3);            
Crb = [ 0           0           -m*(xg*r+v); ...
        0           0           -m*(yg*r-u); ...
        m*(xg*r+v)  m*(yg*r-u)  0];
% MA has negative erntries
CA =  [ 0                               0                   (abs(vessel.MA(2,2))*vr+abs(vessel.MA(2,3))*rr); ...
        0                               0                   vessel.MA(1,1)*ur; ...
        (vessel.MA(2,2)*vr+vessel.MA(2,3)*rr) abs(vessel.MA(1,1))*ur 0];

vessel.tau3_CC = -(Crb*actVlc + CA*vessel.nu3r);

% external forces - propulsion system
tau3            = vessel.tau3_CC + uin; %+vessel.tau3_HL 

% ODE: M*nu' = tau --> nu' = M^-1*tau
nu3_dot         = vessel.Minv*tau3;                     
eta3_dot        = R*actVlc;
xdot            = [eta3_dot; nu3_dot];  