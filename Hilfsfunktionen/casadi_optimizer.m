classdef casadi_optimizer < matlab.System & matlab.system.mixin.Propagates
    
    properties
        % Public, tunable properties.

    end
    properties (DiscreteState)
    end
    properties (Access = private)
        casadi_solver
        x0
        lb
        ub
        lbg
        ubg
        nInputs
        nStates
        t
        u
        lbx
        ubx
        pre
        optInput
        CumControlVar
        ControlLimits
        tSampling
        tOutput
    end
    methods (Access = protected)
        function num = getNumInputsImpl(~)
            num = 4;
        end
        function num = getNumOutputsImpl(~)
            num = 3;
        end
        function [dt1, dt2, dt3] = getOutputDataTypeImpl(~,~)
        	dt1 = 'double';
            dt2 = 'double';
            dt3 = 'double';
        end
        function dt1 = getInputDataTypeImpl(~)
        	dt1 = 'double';
        end  
        function sz1 = getInputSizeImpl(~)
        	sz1 = [6,1];
            sz2 = [1,1];
            sz3 = [3,1];
        end
        function cp1 = isInputComplexImpl(~)
        	cp1 = false;
        end
        function [cp1, cp2, cp3] = isOutputComplexImpl(~)
        	cp1 = false;
            cp2 = false;
            cp3 = false;
        end
        function fz1 = isInputFixedSizeImpl(~)
        	fz1 = true;
        end
        function [fz1, fz2, fz3] = isOutputFixedSizeImpl(~,~)
        	fz1 = true;
            fz2 = true;
            fz3 = true;
        end
        function [sz1,sz2,sz3] = getOutputSizeImpl(obj)
            if (~isempty(propagatedInputSize(obj, 1)))
                nStates     = prod(propagatedInputSize(obj, 1));
                nPrediction = prod(propagatedInputSize(obj, 3));
                nInputs     = 3;                                    % edit manually !!!
                sz1         = [nInputs, 1];
                sz2         = [nPrediction/nStates , nStates+nInputs];
            else
                nInputs     = 3;                                    % edit manually !!!
                sz1         = [nInputs, 1];
                sz2         = [];
            end   
            sz3 = [3,1];
        end
        function setupImpl(obj,~,~,~,~,~)
            % Ellipsengleichung: ((E_In - E_c_e)*cos(-psi_e) - (N_In - N_c_e) * sin(-psi_e))^2/(a_e)^2 + ((E_In - E_c_e)*sin(-psi_e) + (N_In - N_c_e) * cos(-psi_e))^2/(b_e)^2 <=1
            import casadi.*
           % options     = Initalvalues();
            options     = evalin('base', 'options');
            x           = MX.sym('x',6,1);
            u           = MX.sym('u',3,1);
            p           = MX.sym('p',6,1);
           % o           = MX.sym('o',8,1);
           
            
           %% LÖSCHEN
            options.MPC.staticObstacles = table2struct(readtable('statischeHindernisse.csv'));
            options.MPC.numStaticObst   = length(options.MPC.staticObstacles);
            %%
            nStates     = options.Initial.nStates;
            nInputs     = options.Initial.nInputs;
            
            x0          = [options.Initial.p_n; options.Initial.Theta_nb; options.Initial.v_b; options.Initial.omega_b];
            
            vessel      = initializeVessel();
            xdot        = runVessel(options, x, u);
            % xdot        = ShipModelMPC(x,u,options);
            J           = options.MPC.Jdx * (p(1)-x(1))^2 ...
                        + options.MPC.Jdy * (p(2)-x(2))^2 ...
                        + options.MPC.Jdpsi * ssa(p(3)-x(3),'rad')^2 ...
                        + options.MPC.Jdu * (p(4)-x(4))^2 ...
                        + options.MPC.Jdv * (p(5)-x(5))^2 ...
                        + options.MPC.Jdw * ssa(p(3)-x(3),'rad')^2 ...
                        + options.MPC.JFx * (u(1)/options.MPC.uUpperLimit(1))^2 ...
                        + options.MPC.JFy * (u(2)/options.MPC.uUpperLimit(2))^2 ...
                        + options.MPC.JFn * (u(3)/options.MPC.uUpperLimit(3))^2;

          
            % Create CasADI functions
            fxdot 	= Function('fxdot', {x,u}, {xdot});
            fJ      = Function('fJ', {x,u,p}, {J});

            %% Integration/discretization using RK4
            xStart  = MX.sym('xStart',nStates,1);   % Initial condition of integration
            u       = MX.sym('u',nInputs,1);        % Control input
            TRK4    = options.MPC.Ts/options.MPC.nRK4;      % Step size of each RK4 interval

            % Loop over intervals
            xEnd    = xStart;                       % Initialization
            JEnd    = 0;                            % Initialization
            
            % obstacle pre calculation
            nOb     = options.MPC.nObst;
            nValOb  = options.MPC.nObstVal;
           
            
            %%
            for l = 1:options.MPC.nRK4  % Runge Kutta Integration
                k1x = fxdot(xEnd, u);
                k2x = fxdot(xEnd + TRK4/2 * k1x, u);
                k3x = fxdot(xEnd + TRK4/2 * k2x, u);
                k4x = fxdot(xEnd + TRK4 * k3x, u);
                k1J = fJ(xEnd, u, p);
                k2J = fJ(xEnd + TRK4/2 * k1x, u,p);
                k3J = fJ(xEnd + TRK4/2 * k2x, u,p);
                k4J = fJ(xEnd + TRK4 * k3x, u,p);
                xEnd = xEnd + TRK4/6 * (k1x + 2*k2x + 2*k3x + k4x);
                JEnd = JEnd + TRK4/6 * (k1J + 2*k2J + 2*k3J + k4J);
            end % for

            % Create CasADi functions
            fxDisc  = Function('fxDisc', {xStart,u}, {xEnd});
            fJDisc  = Function('fJDisc', {xStart,u,p}, {JEnd});


                         % "Lift" initial conditions
            % Initialization
            optVars     = [];   % Vector of optimization variables (i.e. states and inputs)
            optInput    = [];   % Boolean Vector if optimization variable is control variable
            optVars0    = [];   % Initial guess for optimization variables
            lb          = [];   % Lower bound of optimization variables
            ub          = [];   % Upper bound of optimization variables
            Jk          =  0;   % Initialization of objective function
            g           = [];   % (In-)equality constraints
            lbg         = [];   % Lower bound on (in-)equality constraints
            ubg         = [];   % Upper bound on (in-)equality constraints

            % Pre-define CasADi variables
            U = MX.sym('U_',nInputs,1,floor(options.MPC.N));
            S = MX.sym('S_',nStates,1,floor(options.MPC.N+1));
            P = MX.sym('P_',nStates * floor(options.MPC.N+1));
            O = MX.sym('O_',nOb,nValOb);
            
            

            
            % Construct NLP step-by-step
           
            i = 1;
            for k = 1:floor(options.MPC.N+1)
                % System dynamics and objective function
                if k== floor(options.MPC.N+1)
                    % Skip, because at final time step, no further integration of
                    % system dynamics necessary.
                else
                    % Integration of system dynamics and objective function
                    if k==1
                        % Hardcoded initial condition
                        X0          = MX.sym('X0', nStates);
                        optVars     = [optVars; X0];
                        optInput    = [optInput; zeros(nStates,1)];
                        optVars0    = [optVars0; x0]; 
                        lb          = [lb; x0];
                        ub          = [ub; x0];
                        XEnd        = fxDisc(X0,U{1});
                        Jk          = Jk + fJDisc(X0,U{1},P{1:nStates});
                    else
                        XEnd        = fxDisc(S{k},U{i});
                        Jk          = Jk + fJDisc(S{k},U{i},P{((k-1)*nStates+1):k*nStates});
%                         for j = 0:nOb-1
%                             ObId                = j * nValOb + 1;
%                             [N_Check, E_Check]  = rotTransFunc(S{k}(1,1), S{k}(2,1), S{k}(3,1), O(j,6), O(j,7));
%                             g                   = [g; ((E_Check - O(j,2))*cos(-O(j,3)) - (N_Check - O(j,1)) * sin(-O(j,3)))^2/(O(j,4))^2 + ((E_Check - O(j,4))*sin(-O(j,3)) + (N_Check - O(j,1)) * cos(-O(j,3)))^2/(O(j,5))^2];
%                             lbg                 = [lbg; ones(1,1)];
%                             ubg                 = [ubg; inf(1,1)];
%                         end
                        
%                          g           = [g; ((S{k}(2,1) - E_c_e)*cos(-psi_e) - (S{k}(1,1) - N_c_e) * sin(-psi_e))^2/(a_e)^2 + ((S{k}(2,1) - E_c_e)*sin(-psi_e) + (S{k}(1,1) - N_c_e) * cos(-psi_e))^2/(b_e)^2];
%                         lbg         = [lbg; ones(1,1)];
%                         ubg         = [ubg; inf(1,1)];
                        
                    end % if
                        % Add equality constraint for continuity (i.e. closing gaps):
                        g           = [g; XEnd-S{k+1} ];
                        lbg         = [lbg; zeros(nStates,1)];
                        ubg         = [ubg; zeros(nStates,1)];
                    

                end % if

                % States
                if k==1
                    % Skip, because we have hardcoded the initial condition above
                else
                    % Add states to vector of optimization variables
                    optVarsold      = optVars;
                    optVars         = [optVarsold; S{k}];
                    optInput        = [optInput; zeros(nStates,1)];

                    % Lower- and upper bounds for states
                    lb              = [lb; -inf(nStates,1)];
                    ub              = [ub; inf(nStates,1)];

                    % Add initial guess of states
                    optVars0        = [optVars0; x0];

                end % if

                % Inputs
                if k==options.MPC.N+1
                    % Skip, because no control input at final time step
                else
                    % Add inputs to vector of optimization variables
                    if options.MPC.ControlVar(k)
                        optVarsold  = optVars;
                        optVars     = [optVarsold; U{i}];
                        optInput    = [optInput; ones(nInputs,1)];

                        % Lower- and upper bounds for inputs
                        lb          = [lb; options.MPC.uLowerLimit'];
                        ub          = [ub; options.MPC.uUpperLimit'];
                        
                        % Add initial guess for inputs
                        optVars0    = [optVars0; zeros(nInputs,1)];
                        i           = i + 1;
                        if i >= options.MPC.SumControlVar
                            i       = options.MPC.SumControlVar;
                        end
                    end
                end % if

            end % for
            
            % Construct Solver
            prob                = struct('f', Jk, 'x', optVars, 'g', g, 'p', P);
            optionsMPC          = struct('ipopt',struct('max_iter',options.MPC.maxIter,'acceptable_tol',options.MPC.acceptable_tol));
            solver              = nlpsol('solver', 'ipopt', prob, optionsMPC);

            %% Code Gen
            ng         = length(g);
            nOptVars   = length(optVars);
            lamEq_     = casadi.MX.sym('lambda', ng);

            lag_       = Jk - g.'*lamEq_;
            qpH_       = hessian(lag_, optVars);
            qpf_       = gradient(Jk, optVars);
            qpb_       = -g;
            qpA_       = jacobian(g, optVars);
            
            sqpPrepFun = casadi.Function( 'Ship_sqpPrep', ...
                       {optVars, lamEq_, P(:)}, ...
                       {qpH_(:), qpf_(:), qpA_(:), qpb_(:), lb, ub});

            % Define codegeneration options
            OptionsCg.main     = true;
            OptionsCg.mex      = true;
           

            % Generate    
           sqpPrepFun.generate('QPShip.c', OptionsCg)
           
            
            %% Save
            obj.casadi_solver   = solver;
            obj.x0              = optVars0;
            obj.optInput        = optInput;
            obj.lbx             = lb;
            obj.ubx             = ub;
            obj.lbg             = lbg;
            obj.ubg             = ubg;
            obj.t               = -1;         
            obj.nInputs         = nInputs;
            obj.nStates         = nStates;
            obj.CumControlVar   = cumsum([options.MPC.ControlVar; 0]);
            obj.ControlLimits   = options.MPC.uUpperLimit; 
            obj.tSampling       = options.MPC.Ts;
            
            
            %% Debugging Schleife
%             xStarter = zeros(nStates,options.MPC.N);
%             for o = 1:options.MPC.N-1
%                 uin = [40000;0;0];
%                xStarter(:,o+1) = full(fxDisc(xStarter(:,o),uin));
%             end
%             xStarter = xStarter';
         end            
             
         function [u, pre, tOutput] = stepImpl(obj,x,t,path, obst)
%              if t >= obj.t
                tic
                nAll                = obj.nInputs+obj.nStates;
                w0                  = obj.x0;
                lbw                 = obj.lbx;
                lbw(1:6)            = x;
                ubw                 = obj.ubx;
                ubw(1:6)            = x;
                solver              = obj.casadi_solver;
                optInput            = obj.optInput;
                p                   = path;
                sol                 = solver('x0', w0, 'lbx', lbw, 'ubx', ubw,'lbg', obj.lbg, 'ubg', obj.ubg, 'p', p);
                optVarsOpt          = full(sol.x);                                
                
                % create new input vector (obj.x0) for next iteration step
                uPredVar            = optVarsOpt(optInput==1);
                xPred               = optVarsOpt(optInput==0);
                x0New               = optVarsOpt;
                x0New(optInput==1)  = [uPredVar(obj.nInputs+1:end); uPredVar(end-obj.nInputs+1:end)];
                x0New(optInput==0)  = [xPred(obj.nStates+1:end); xPred(end-obj.nStates+1:end)];
                obj.x0              = x0New;

                % recreate control input over the horizon
                uPredSplit          = reshape(uPredVar, obj.nInputs, length(uPredVar)/obj.nInputs)';
                uControlpre         = uPredSplit(obj.CumControlVar,:);
                uControlprenorm     = uControlpre./obj.ControlLimits;
                u                   = uControlpre(1,:)';
                
                % create the predicition vector (pre) for plotting
                xpre                = xPred(1:obj.nStates:end);
                ypre                = xPred(2:obj.nStates:end);
                psipre              = xPred(3:obj.nStates:end);
                upre                = xPred(4:obj.nStates:end);
                vpre                = xPred(5:obj.nStates:end);
                rpre                = xPred(6:obj.nStates:end);
                pre                 = [xpre ypre psipre upre vpre rpre uControlprenorm];
                obj.t               = t + obj.tSampling;
                obj.u               = u;
                obj.pre             = pre; 
                elapsedTime         = toc;
                tOutput             = [t; elapsedTime; elapsedTime/obj.tSampling];
                obj.tOutput         = tOutput;

         end
    end
end           
            
            