function options = Initalvalues()
%% Values 
% Initial values
options.Initial.p_n         = [0;0];                    % Position NED [m]
options.Initial.Theta_nb    = 0;                        % Euler angle [rad]
options.Initial.v_b         = [0;0];                    % linear velocity ship [m/s]
options.Initial.omega_b     = 0;                        % angular velocity [rad/s]
options.Initial.u           = [0; 0; 0];                % initial Control
options.Initial.nStates     = 6;                        % number of state variables
options.Initial.nInputs     = 3;                        % number of control variables
options.Initial.x0          = zeros(6,1);
options.Initial.lat0        = 49.997117;                % Reference Point Latitude [�]
options.Initial.lon0        =  8.02045;                % Reference Point Longitude [�]
options.Initial.xLim        = [-400 400];               % X-Limits of the Plot [m] relative to the Reference Point
options.Initial.yLim        = [-400 400];               % Y-Limits of the Plot [m] relative to the Reference Point.


%% MPC
options.MPC.Ts              = 4;                        % Sampling time [s]
options.MPC.tFinal          = 40;                       % Prediction time [s]
options.MPC.tControl        = 40;                       % Control time [s]
options.MPC.reduction       = 1;                        % reduction parameter for control horizon intialization. See section calculation. Set =1 if no reduction is needed.

options.MPC.nRK4            = 1;                        % Number of RK4 intervals per time step
options.MPC.maxIter         = 10;                       % Maximum iterations for solver
options.MPC.acceptable_tol  = 1e-3;                     % optimality convergence tolerance

% Limits input
options.MPC.uLowerLimit     = [-80000 -80000 -10000000];   % F_x [N], F_y [N], N [Nm] 
options.MPC.uUpperLimit     = [80000 80000 10000000];      % F_x [N], F_y [N], N [Nm]
options.MPC.maximumPower    = 1000000;                     % Maximum Power [W]

% Limits states
options.MPC.nObst           = 5;                        % Number of obstacles for each time step
options.MPC.nObstVal        = 8;                        % Number of informations for each obstacle (exp.: N_c_e, E_c_e, a_e, b_E)

% Values objective function
options.MPC.Jdx             = 1;
options.MPC.Jdy             = 1;
options.MPC.Jdpsi           = 10000;

options.MPC.Jdv             = 1;
options.MPC.Jdu             = 1;
options.MPC.Jdw             = 0;

options.MPC.JFx             = 1;
options.MPC.JFy             = 1;
options.MPC.JFn             = 1;

%% Other 
options.boolGoogleMapPlot   = 1;
options.boolCSVPath         = 1;

%% Calculations
options                     = initialCalculations(options);
options.vessel              = initializeVessel();
